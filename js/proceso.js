async function leerJSON(url){
    try{
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch(err) {
        alert(err);
    }
}

var titulo = "";

function consultarNotas(){
    
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json";
    
    leerJSON(url).then(datos => {
      //  console.log(datos);
        //imprimo el titulo
       //titulo+="<br> <h2>Nombre de Materia:" +datos.nombreMateria+"</h2>";
       
        //para poder mostrar el nombre de la materia en la tabla toco guardarla en una variable
        titulo=datos.nombreMateria;   
        
        //para imprirmir en el html un dato
        /*
        var msg="";
        var resultado=document.getElementById("c");
        msg+="<br><h2>Codigo:"+datos.estudiantes[10].codigo +"</h2>";
        resultado.innerHTML=msg;
        */
    /*  console.log(datos.descripcion[1].descripcion);
      console.log(datos.estudiantes[1].nombre);
     */ 
      
      buscarCodigo(datos.estudiantes,datos.descripcion);
      
        //console.log(datos.nombreMateria);
        drawTableNotasEstudiantes(datos.descripcion);
        
 
     
    })
}




function buscarCodigo(estudiantes,descripcion) {
    let codigo = document.getElementById("codigo").value;
    console.log(codigo);

    let eli = document.getElementById("notas").value;
    console.log("notas a eliminar menores a="+eli);
    
    
    for(i=0; i<estudiantes.length;i++){
    
    if (codigo==estudiantes[i].codigo){
        alert("El Codigo ha sido encontrado");
        
        //Imprimir por log los datos que quiera
    //    console.log(i);
   //     console.log(estudiantes[i].nombre);
       
        drawTableNotasEstudiantes(estudiantes,i,descripcion,eli);
        
         
        return i;
        
    } else{
        n="EL codigo NO ha sido Encontrado";
        
    }
    
}

alert(n);  

}  



function drawTableNotasEstudiantes(estudiantes,i,descripcion,eli) {
    var data = new google.visualization.DataTable();
    //var data3 = new google.visualization.DataTable();
    //Encabezados de las tablas:
    //Encabezados tabla 1
    data.addColumn('string', 'nombre: '+estudiantes[i].nombre);
    
    //Crear las filas tabla 1:
    data.addRows(2);
    data.setCell(0, 0, 'Codigo:'+ estudiantes[i].codigo);
    data.setCell(1, 0, 'Materia:'+ titulo);


    var table = new google.visualization.Table(document.getElementById('table_div1'));
    table.draw(data, { showRowNumber: false, width: '50%', height: '100%' });
 
    console.log("imprimiendo grafico 1");
    console.log("imprimo i desde grafica 1= "+i);
    console.log("estudiantes.length="+estudiantes.length);
    console.log("descripcion.length="+descripcion.length);
  
  
    drawTablaNotas(estudiantes,i,descripcion,eli);

}





function drawTablaNotas (estudiantes,i,descripcion,eli){

    var data2 = new google.visualization.DataTable();

      // crearEncabezados2(data2);
      data2.addColumn('string', 'Descripcion: ');
      data2.addColumn('number', 'Nota Obtenida: ');
      data2.addColumn('string', 'Observasion: ');
  
  
      console.log("imprimo i desde grafica 2="+i);
       console.log("Eliminar desde tabla notas="+eli);
      

      //
      //Prueba eliminar 
  /*   
*/



      data2.addRows(descripcion.length+1);
      //imprimir nombre de cada trabajo
      obs="";
      var prom=0;
      aprobadas=0;
      noaprobadas=0;
      for (j=0 ; j<descripcion.length; j++){
          data2.setCell(j, 0, descripcion[j].descripcion);
          data2.setCell(j, 1, estudiantes[i].notas[j].valor);
          
        
         
        
          prom+=(estudiantes[i].notas[j].valor)/descripcion.length;
          



         /* if(estudiantes[i].notas[j].valor >= 3.0){
              obs="Nota aprobada";
              aprobadas++;
          }
          else {
              obs="Nota reprobada";
          }
*/
//de aqui empiezo a elimar nota por "eli"


if (estudiantes[i].notas[j].valor >= 3.0){
    obs="Nota aprobada";
    aprobadas++;

    if(eli>=0 && eli<=5){
        if(eli >= estudiantes[i].notas[j].valor){
    //      2       1.5
            obs="Nota Eliminada";
            aprobadas--;
        }
        
    }



}
else {
    obs="Nota reprobada";
    noaprobadas++;
    if(eli>=0 && eli<=5){
        if(eli >= estudiantes[i].notas[j].valor){
    //      2       1.5
            obs="Nota Eliminada";
            noaprobadas--;
        }
        
    }
}

//imprimo en obs la nota eliminada




//a qui que imprime el obs en la celda


          data2.setCell(j, 2, obs);
      }
      console.log("notas Aprobadas="+aprobadas);

      //probando
verAutoevaluacion(prom);
      
     // noaprobadas=descripcion.length-aprobadas;
      console.log("Cantidad de no aprobadas="+noaprobadas);
      data2.setCell(j, 0, "NOTA DEFINITIVA");
      data2.setCell(j, 1, prom); 
      obsdef="";
      
      if(prom>=3.0){
          obsdef="Nota Aprobada";
        }
        else{
            obsdef="Nota Reprobada";
        }

      data2.setCell(j, 2, obsdef);   
      console.log("j="+j);
      console.log(prom);
      
      /*
       //Crear las filas tabla 2:
      data2.addRows(2);

    data2.setCell(0, 0, 'Codigo:');
    data2.setCell(0, 1, '1:');
    data2.setCell(0, 2, '2');
    data2.setCell(1, 0, '0:');
    data2.setCell(1, 1, '1');
    data2.setCell(1, 2, '2:');
    */
    

     

      var table2 = new google.visualization.Table(document.getElementById('table_div2'));
      table2.draw(data2, { showRowNumber: false, width: '50%', height: '100%' });

      crearGrafo(aprobadas,noaprobadas);
      
}




function crearGrafo(aprobadas,noaprobadas){

    var data3 = new google.visualization.DataTable();


    data3.addRows(2);

    data3.addColumn('string', 'Descripción'); //0
    data3.addColumn('number', 'Valor'); //1
    
    

    console.log("Aprobadas="+aprobadas);
    console.log("NO aprobadas="+noaprobadas);

    data3.setCell(0,0,"Aprobados");
    data3.setCell(0,1,aprobadas);
    data3.setCell(1,0,"Reprobados");
    data3.setCell(1,1,noaprobadas);

    var options = {
      title: 'Estadística de Notas',
      is3D: true,
    };



    var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
    chart.draw(data3, options);






}


function verAutoevaluacion(prom){
    

 alert("Mi nota es de : "+ prom +"<br> Esta nota es debido a: Etiam et justo ac ante elementum aliquam. Orci varius natoque penatibus et magnis dis parturient montes");
}
